import MiniCssExtractPlugin from "mini-css-extract-plugin"
import HtmlWebpackPlugin from "html-webpack-plugin";
import path from "path";

const config = {
    devtool: "source-map",
    mode: "development",

    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve("src/html/index.html"),
        }),
        new MiniCssExtractPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.html?$/i,
                use: ['html-loader']
            },
            {
                test: /\.css$/i,
                use: [MiniCssExtractPlugin.loader, "css-loader"]
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                type: "asset/resource"
            },

        ]
    },
    devServer: {
        hot: false,
        liveReload: true, // live reloading enablen
        static: {directory: path.resolve("dist")},
        open: true
    },
    output: {
        clean: true, //maakt de 'dist' folder leeg alvorens nieuwe files te genereren.
    },
}
export default config
